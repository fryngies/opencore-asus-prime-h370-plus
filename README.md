# OpenCore ASUS Prime H370 Plus config

Supported OpenCore version: 0.5.6.

Test PC config: ASUS Prime H370 Plus, i5-8500, Gigabyte Radeon RX 580 Gaming 8GB, Crucial 16GBx2 2666MHz running in dual channel, SATA SSD && SATA HDD.

As of time of writing, I use it as my daily PC (work included).

## What works

- Dual monitor setup (DVI + HDMI, didn't care about VGA)
- Audio, including HDMI
- USB
- Sleep
- Intel Turbo Boost
- iGPU
- Everything else you could think of?

## Prerequirements

Initially I used internal GPU, but now I switched to RX580. Just bear in mind that I didn't properly tested it with current revision, but it used to work and I didn't removed any fixes (yet). Only change that needs to be made is changing model to `iMac19,1` and `ig-platform-id` to `0300923E` as per [guide I used](https://khronokernel-2.gitbook.io/opencore-vanilla-desktop-guide/intel-config.plist/coffee-lake#deviceproperties).

### [Generate your own SSDT-UIAC](https://usb-map.gitbook.io/project/)

While SSDT presented in this repo may work for you, I didn't bother to set every available USB port on the motherboard, only one I use. This is not hard, so don't be afraid.

### Generate your own serial and board number

### BIOS settings

- Disable:
	- Fast Boot
	- VT-d
	- CSM
	- SGX
	- CFG Lock
	- Serial Port (fsr fixed USB sound card shuttering)
- Enable:
	- Virtualization if needed
	- 4G decoding
	- XHCI hand-off
- Either enable, set to auto or to only available option everything under PCI (power) settings to fix network chip beign stalled sometimes.

## Known issues

- Some shades of grey are "blinking" when using iGPU
- Graphic glitches on login screen after boot for a few seconds when using dual monitor setup when using iGPU

## TODO

- [ ] Fix known issues
- [ ] Filevault?
- [ ] Test iServices

## Credits

- [OpenCore vanilla desktop guide](https://khronokernel-2.gitbook.io/opencore-vanilla-desktop-guide/)
- [OpenCore developers](https://github.com/acidanthera/OpenCorePkg)

